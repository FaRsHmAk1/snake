// Fill out your copyright notice in the Description page of Project Settings.


#include "KillZone.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "SnakeBase.h"
#include "Food.h"

// Sets default values
AKillZone::AKillZone()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	KillZone = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("KillZone"));
}

// Called when the game starts or when spawned
void AKillZone::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AKillZone::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AKillZone::Interact(AActor* Interactor, bool bIsHead)
{

	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			for(ASnakeElementBase* element : Snake->SnakeElement)
			{
				element->Destroy();
			}
			Snake->Destroy();
		}
		
	}
}
