// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Food.h"
#include "KillZone.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = .5f;
	LastMoveDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	AddSnakeElement(5);
	
	
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
	SetActorTickInterval(MovementSpeed);
}





void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation(SnakeElement.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElemement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElmentClass, NewTransform);
		NewSnakeElemement->SnakeOwner = this;
		NewSnakeElemement->SetActorHiddenInGame(true);
		int32 ElemIndex = SnakeElement.Add(NewSnakeElemement);
		if (ElemIndex == 0)
		{
			NewSnakeElemement->SetFirstElementType();
		}
	}	
}

void ASnakeBase::Move()
{
	FVector MovementVector(0);
	

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
			break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
			break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
			break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
			break;
	}
	
	//AddActorWorldOffset(MovementVector);
	SnakeElement[0]->ToggleCollision();

	for(int i = SnakeElement.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElement[i];
		auto PrevElement = SnakeElement[i - 1];
		SnakeElement[0]->SetActorHiddenInGame(false);
		SnakeElement[i]->SetActorHiddenInGame(false);
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElement[0]->AddActorWorldOffset(MovementVector);
	SnakeElement[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		
		int32 ElemIndex;
		SnakeElement.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
		if (MovementSpeed > .2f)
		{
			MovementSpeed = MovementSpeed - .01f;
		}
	}
}